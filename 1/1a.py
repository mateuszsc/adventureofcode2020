with open('input.txt', 'r') as file:
	lines = file.readlines()
	lines = list(map(lambda x: int(x.strip()), lines))

for x1 in lines:
	x2 = 2020 - x1
	if x2 in lines:
		print(x1*x2)
		break

# answer is 651651