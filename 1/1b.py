with open('input.txt', 'r') as file:
	lines = file.readlines()
	lines = list(map(lambda x: int(x.strip()), lines))

def find():
	for x1 in lines:
		for x2 in lines:
			for x3 in lines:
				if (x1 + x2 + x3) == 2020:
					print(x1*x2*x3)
					return

find()

# answer is 214486272