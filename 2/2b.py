with open('input.txt', 'r') as file:
	lines = file.readlines()
	lines = list(map(lambda x: x.strip(), lines))

policies = []
for line in lines:
	parts = line.split(' ')
	policy = {
		'letter': parts[1][:1],
		'pos1': int(parts[0].split('-')[0]),
		'pos2': int(parts[0].split('-')[1]),
		'pass': parts[2]
	}
	policies.append(policy)

valid = 0
for policy in policies:
	internal_valid = 0
	if policy['pass'][policy['pos1'] - 1] == policy['letter']:
		internal_valid+=1
	if policy['pass'][policy['pos2'] - 1] == policy['letter']:
		internal_valid+=1

	if internal_valid == 1:
		valid+=1

print(valid)

# aswer is 489