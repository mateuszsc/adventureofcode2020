with open('input.txt', 'r') as file:
	lines = file.readlines()
	lines = list(map(lambda x: x.strip(), lines))

policies = []
for line in lines:
	parts = line.split(' ')
	policy = {
		'letter': parts[1][:1],
		'min': int(parts[0].split('-')[0]),
		'max': int(parts[0].split('-')[1]),
		'pass': parts[2]
	}
	policies.append(policy)

valid = 0
for policy in policies:
	count = policy['pass'].count(policy['letter'])
	if count >= policy['min'] and count <= policy['max']:
		valid+=1

print(valid)

# answer is 538