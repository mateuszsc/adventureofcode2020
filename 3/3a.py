np_map = []
with open('input.txt', 'r') as file:
	for line in file.readlines():
		line = line.strip()
		row = []
		for char in line:
			row.append(char)
		np_map.append(row)

x = 0
trees = 0
row_len = len(np_map[0])
for y, row in enumerate(np_map):
	if row[x] == '#':
		trees+=1
	
	x+=3
	if (x >= row_len):
		x -= row_len

print(trees)

# asnwer is 164