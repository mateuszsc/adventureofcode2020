def calculate_trees_amount(map_data, x_move, y_move):
	x = 0
	trees = 0
	row_len = len(map_data[0])
	for y in range(0, len(map_data), y_move):
		if map_data[y][x] == '#':
			trees+=1
	
		x+= x_move
		if (x >= row_len):
			x -= row_len

	return trees


np_map = []
with open('input.txt', 'r') as file:
	for line in file.readlines():
		line = line.strip()
		row = []
		for char in line:
			row.append(char)
		np_map.append(row)

total_trees = 1
for slope in [[1, 1],[3, 1],[5, 1],[7, 1], [1,2]]:
	total_trees *= calculate_trees_amount(np_map, slope[0], slope[1])

print(total_trees)

# asnwer is 5007658656