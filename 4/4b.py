import re


passports = []
with open('input.txt', 'r') as file:
	passport = {}
	for line in file.readlines():
		line = line.strip()
		if line == '':
			passports.append(passport)
			passport = {}
			continue

		for data in line.split(' '):
			key, value = data.split(':')
			# just ingore cid here
			if key == 'cid':
				continue
			elif key == 'byr' and (int(value) < 1920 or int(value) > 2002):
				continue
			elif key == 'iyr' and (int(value) < 2010 or int(value) > 2020):
				continue
			elif key == 'eyr' and (int(value) < 2020 or int(value) > 2030):
				continue
			elif key == 'hgt':
				if value.find('cm') != -1:
					min_v = 150
					max_v = 193
				elif value.find('in') != -1:
					min_v = 59
					max_v =76
				else:
					continue
				
				if int(value[:-2]) < min_v or int(value[:-2]) > max_v:
					continue
			elif key == 'hcl':
				if len(value) != 7:
					continue
				if value[0] != '#':
					continue
				if not re.match(r'^[0-9a-f]*$', value[1:]):
					continue
			elif key == 'ecl' and value not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
				continue
			elif key == 'pid' and len(str(value)) != 9:
				continue

			passport[key] = value
	#quick fix: add last passport
	passports.append(passport)

valid = 0
for passport in passports:
	if len(passport) == 7:
		# debug stuff
		# with open('4b.debug.txt', 'a+') as fi:
		# 	fi.write('{} '.format(passport['byr']))
		# 	fi.write('{} '.format(passport['iyr']))
		# 	fi.write('{} '.format(passport['eyr']))
		# 	fi.write('{} '.format(passport['hgt'].zfill(5)))
		# 	fi.write('{} '.format(passport['hcl']))
		# 	fi.write('{} '.format(passport['ecl']))
		# 	fi.write('{}\n'.format(passport['pid']))
		valid+=1
		continue

print(valid)

# answer is 111
