passports = []
with open('input.txt', 'r') as file:
	passport = {}
	for line in file.readlines():
		line = line.strip()
		if line == '':
			passports.append(passport)
			passport = {}
			continue

		for data in line.split(' '):
			key, value = data.split(':')
			# just ingore cid here
			if key == 'cid':
				continue
			passport[key] = value
	#quick fix: add last passport
	passports.append(passport)

valid = 0
for passport in passports:
	if len(passport) == 7:
		valid+=1
		continue

print(valid)

# answer is 233
